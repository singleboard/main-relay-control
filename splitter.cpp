// g++ -Wall splitter.cpp -o splitter -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi
//
// Created by Antonchikov Alexander on 27.01.2021.
//

#include <iostream>
#include <string>
#include <algorithm>    // copy
#include <iterator>     // back_inserter
#include <regex>        // regex, sregex_token_iterator
#include <vector>

int main()
{

    std::stringstream test("this_is_a_test_string");
    std::string segment;
    std::vector<std::string> seglist;

    while(std::getline(test, segment, '_'))
    {
        seglist.push_back(segment);
        std::cout << segment << std::endl;
    }
//    std::string str = "08/04/2012";
//    std::vector<std::string> tokens;
//    std::regex re("\\d+");
//
//    //start/end points of tokens in str
//    std::sregex_token_iterator
//            begin(str.begin(), str.end(), re),
//            end;
//
//    std::copy(begin, end, std::back_inserter(tokens));
}