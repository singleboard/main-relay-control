#ifndef MYTIME_H
#define MYTIME_H

#include <stdint.h>
#include <stdio.h>

class MyTimeClass {

public:
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;

    MyTimeClass(uint8_t hours, uint8_t minutes, uint8_t seconds);
    ~MyTimeClass();
    void update(uint8_t hours, uint8_t minutes, uint8_t seconds);

    int toSeconds();

    void printSerial();

    static bool between(MyTimeClass* startTime, MyTimeClass* stopTime, MyTimeClass* t);
};


class MyTimeRangeClass {

    private:
        MyTimeClass* startTime;
        MyTimeClass* stopTime;

    public:
        MyTimeRangeClass(uint8_t h1, uint8_t m1, uint8_t s1,
                         uint8_t h2, uint8_t m2, uint8_t s2);

        ~MyTimeRangeClass();

        bool contain(MyTimeClass* t);

        void printSerial();

};


#endif //MYTIME_H
