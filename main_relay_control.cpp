// systemctl restart ds18b20.service
// g++ -Wall main_relay_control.cpp MyTime.cpp -o main_relay_control -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi -ljsoncpp

#include <wiringPi.h>
#include "MyTime.h"

#include <fstream>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.

#include <iostream>
#include <cstdlib>
#include <string>
#include <cstring>
#include <cctype>
#include <thread>
#include <chrono>
#include "mqtt/async_client.h"

#include <algorithm>    // copy
#include <iterator>     // back_inserter
#include <regex>        // regex, sregex_token_iterator
#include <vector>
#include <list>
#include <ctime>

#define PIN_FREE 0
#define PIN_COMPRES 2
#define PIN_COND 3
#define PIN_FAN 22
#define PIN_PUMP 23
#define PIN_LAMP 24 // ANSWERPRO/LAMP

using namespace std;

const string SERVER_ADDRESS	{ "tcp://localhost:1883" };
const string CLIENT_ID		{ "async_consume" };
//const string TOPIC 			{ "answerpro/+/temp" };
const string TOPIC 			{ "ANSWERPRO/#" };

const int  QOS = 1;

int LAMP_TEMP_MAX = 40;
double LAMP_TEMP = 22;

/////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    list <MyTimeRangeClass*> mylist;

    if (wiringPiSetup() == -1) { //If wiringpi initialization failed, printf
        printf( "Wiring Pi Initialization Faild");
        return 1;
    }
    pinMode(PIN_FREE, OUTPUT);
    pinMode(PIN_COMPRES, OUTPUT);
    pinMode(PIN_COND, OUTPUT);
    pinMode(PIN_FAN, OUTPUT);
    pinMode(PIN_PUMP, OUTPUT);
    pinMode(PIN_LAMP, OUTPUT);

    mqtt::connect_options connOpts;
	connOpts.set_keep_alive_interval(20);
	connOpts.set_clean_session(true);

	mqtt::async_client cli(SERVER_ADDRESS, CLIENT_ID);

	try {
		cout << "Connecting to the MQTT server..." << flush;
		cli.connect(connOpts)->wait();
		cli.start_consuming();
		cli.subscribe(TOPIC, QOS)->wait();

		cout << "OK" << endl;

		// Consume messages

		while (true) {

			auto msg = cli.consume_message();
			if (!msg) break;
			string topic = msg->get_topic();
			string message = msg->to_string();

            stringstream streamTopic(topic.c_str());
            string segment;
            vector<string> vectorTopic;

            while(std::getline(streamTopic, segment, '/'))
            {
                vectorTopic.push_back(segment);
            }
            cout << vectorTopic.at(0) << "/" << vectorTopic.at(1) << ":" << message << endl;
            if(vectorTopic.at(1) == "LAMP") {
                if(message == "on")
                    digitalWrite(PIN_LAMP, HIGH);
                else if(message == "off")
                    digitalWrite(PIN_LAMP, LOW);
            }
            if(vectorTopic.at(1) == "PUMP") {

                if(message == "on")
                    digitalWrite(PIN_PUMP, HIGH);
                else if(message == "off" && LAMP_TEMP > LAMP_TEMP_MAX)
                    digitalWrite(PIN_PUMP, LOW);
            }
            if(vectorTopic.at(1) == "FAN") {
                if(message == "on")
                    digitalWrite(PIN_FAN, HIGH);
                else if(message == "off")
                    digitalWrite(PIN_FAN, LOW);
            }
            if(vectorTopic.at(1) == "COND") {
                if(message == "on")
                    digitalWrite(PIN_COND, HIGH);
                else if(message == "off")
                    digitalWrite(PIN_COND, LOW);
            }
            if(vectorTopic.at(1) == "COMPRES") {
                if(message == "on")
                    digitalWrite(PIN_COMPRES, HIGH);
                else if(message == "off")
                    digitalWrite(PIN_COMPRES, LOW);
            }
            if(vectorTopic.at(1) == "FREE") {
                if(message == "on")
                    digitalWrite(PIN_FREE, HIGH);
                else if(message == "off")
                    digitalWrite(PIN_FREE, LOW);
            }
            if(vectorTopic.at(1) == "LAMP_TEMP_MAX") {
                LAMP_TEMP_MAX = stoi(message);
                cout << "LAMP_TEMP_MAX:" << LAMP_TEMP_MAX << endl;
            }
            if(vectorTopic.at(1) == "DS18B20" && vectorTopic.at(2) == "1" && vectorTopic.at(3) == "TEMP") {
                LAMP_TEMP = stod(message);
                cout << "LAMP_TEMP:" << LAMP_TEMP << endl;
            }

            if(vectorTopic.at(1) == "LAMP_SCHEDULER") {
                mylist.clear();
                Json::Value root;
                Json::Reader reader;
                bool parsingSuccessful = reader.parse( message, root );
                if ( !parsingSuccessful )
                {
                    cout << "Error parsing the string" << endl;
                }
                Json::FastWriter fastWriter;
                string output = fastWriter.write(root["timers"]);
                cout << "1: " << output << endl;
                Json::Value timers = root["timers"];


                for( Json::Value::const_iterator outer = timers.begin() ; outer != timers.end() ; outer++ )
                {
                    Json::Value obj = (*outer);
                    uint8_t starthour = obj["starthour"].asUInt();
                    uint8_t endhour = obj["endhour"].asUInt();
                    uint8_t startmin = obj["startmin"].asUInt();
                    uint8_t endmin = obj["endmin"].asUInt();
                    MyTimeRangeClass* timeRange = new MyTimeRangeClass(starthour, startmin, 0, endhour, endmin, 0);
                    mylist.push_back(timeRange);
                }

                list <MyTimeRangeClass*> :: iterator it;
                for (it = mylist.begin(); it != mylist.end(); it++) {
//                    cout << (*it) << " ";
                    (*it)->printSerial();
                }
//                MyTimeRangeClass* timeRange = new MyTimeRangeClass(starthour, startmin, 0, endhour, endmin, 0);
//                timeRange->printSerial();
//                Serial.println();
//                myLinkedList->add(timeRange);

            }


//          CHECK CURRENT TIME AND LAMP IF SUCCESS THEN SWITCH ON
            std::time_t t = std::time(0);   // get time now
            std::tm* now = std::localtime(&t);
            std::cout << (now->tm_hour) << '-'
                      << (now->tm_min) << '-'
                      <<  now->tm_sec
                      << "\n";
            MyTimeClass* currentTime = new MyTimeClass(now->tm_hour, now->tm_min, now->tm_sec);
            list <MyTimeRangeClass*> :: iterator it;
            bool flag = false;
            for (it = mylist.begin(); it != mylist.end(); it++) {
                MyTimeRangeClass* timeRange = (*it);
                if(timeRange->contain(currentTime)) {
                    flag = true;
                }
            }
            int lamp_out = digitalRead (PIN_LAMP) ;
            cout << "lamp_out" << lamp_out << endl;
            if(flag) {
                cout << "FLAG" << endl;
                if(lamp_out == LOW) {
                    string TOPIC_LAMP {"ANSWERPRO/LAMP"};
                    string LAMP_ON = "on";
                    const char* PAYLOAD2 = "on";
                    cli.publish(TOPIC, PAYLOAD2, strlen(PAYLOAD2), 0, false);
                }
            }
            else {
                if(lamp_out == HIGH) {
                    string TOPIC {"ANSWERPRO/LAMP"};
                    const char* PAYLOAD2 = "off";
                    cli.publish(TOPIC, PAYLOAD2, strlen(PAYLOAD2), 0, false);
                }
            }

		}

		// Disconnect
		cout << "\nShutting down and disconnecting from the MQTT server..." << flush;
		cli.unsubscribe(TOPIC)->wait();
		cli.stop_consuming();
		cli.disconnect()->wait();
		cout << "OK" << endl;
	}
	catch (const mqtt::exception& exc) {
		cerr << exc.what() << endl;
		return 1;
	}

 	return 0;
}

