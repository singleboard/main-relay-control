// g++ -Wall MyTime.cpp -o mytime


#include "MyTime.h"
#include <iostream>
#include <sstream>

using namespace std;

MyTimeClass::MyTimeClass(uint8_t hours = 0, uint8_t minutes = 0, uint8_t seconds = 0) {
    this->hours = hours;
    this->minutes = minutes;
    this->seconds = seconds;
}

MyTimeClass::~MyTimeClass() {
}

void MyTimeClass::update(uint8_t hours = 0, uint8_t minutes = 0, uint8_t seconds = 0) {
    this->hours = hours;
    this->minutes = minutes;
    this->seconds = seconds;
}

int MyTimeClass::toSeconds() {
    return 3600*this->hours + 60*this->minutes + this->seconds;
}

void MyTimeClass::printSerial() {
    cout << "Time:" << (int)this->hours << ":" << (int)this->minutes << ":" << (int)this->seconds << endl;
}

bool MyTimeClass::between(MyTimeClass* startTime, MyTimeClass* stopTime, MyTimeClass* t) {
    cout << "between" << endl;
    return true;
}


/////////////////////////////////////////////////////////////////////////////////////
MyTimeRangeClass::MyTimeRangeClass(uint8_t h1 = 0, uint8_t m1 = 0, uint8_t s1 = 0,
                 uint8_t h2 = 0, uint8_t m2 = 0, uint8_t s2 = 0) {
    this->startTime = new MyTimeClass(h1, m1, s1);
    this->stopTime = new MyTimeClass(h2, m2, s2);
}

MyTimeRangeClass::~MyTimeRangeClass() {
    delete startTime;
    delete stopTime;
}

bool MyTimeRangeClass::contain(MyTimeClass* t) {
    int startSec = this->startTime->toSeconds();
    int stopSec = this->stopTime->toSeconds();
    int iSec = t->toSeconds();
    if(iSec >= startSec && iSec <= stopSec)
        return true;
    else
        return false;
}

void MyTimeRangeClass::printSerial() {
    cout << "StartTime: " << endl;
    startTime->printSerial();
    cout << "StopTime: " << endl;
    stopTime->printSerial();
}


//int main(int argc, char* argv[]) {
//    MyTimeClass *currentTime;
//    currentTime = new MyTimeClass(0, 10, 0);
//    currentTime->printSerial();
//    int sec = currentTime->toSeconds();
//    cout << sec << endl;
//    return 1;
//}